package com.example.starter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LogError {

    boolean swallowException() default false;

    Class<? extends Throwable> exception() default Throwable.class;

    String message();
}
