package com.example.starter.utils;

public class TemplateUtils {
    public static final String METHOD_NAME = "Method name %s";
    public static final String METHOD_RETURN_TYPE = "Method return type %s";
    public static final String METHOD_INFO_INFORMATION = "Method log info information";
    public static final String METHOD_DEBUG_INFORMATION = "Method log debug information";
    public static final String METHOD_ARGUMENT_INFO_VALUE = "Method argument type %s , value = %s";
    public static final String METHOD_RETURNING_VALUE_INFO= "Returning type of value: %s, value = %s";

    public static final String EXCEPTION_WAS_CAUGHT = "Exception %s was caught ";
    public static final String EXCEPTION_WAS_THROWN = "Exception %s was thrown ";

    public static final String VOID_TYPE_METHOD = "Void type method";
}
