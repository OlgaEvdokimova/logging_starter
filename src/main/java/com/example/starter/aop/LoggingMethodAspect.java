package com.example.starter.aop;

import com.example.starter.annotation.LogError;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;

import static com.example.starter.utils.TemplateUtils.*;

@Slf4j
@Aspect
@Component
public class LoggingMethodAspect {


    @Around("@annotation(com.example.starter.annotation.LogInfo)")
    public Object methodInfoAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info(METHOD_INFO_INFORMATION);
        return methodLog(joinPoint, log::info);
    }


    @Around("@annotation(com.example.starter.annotation.LogDebug)")
    public Object methodDebugAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug(METHOD_DEBUG_INFORMATION);
        return methodLog(joinPoint, log::debug);
    }


    private Object methodLog(ProceedingJoinPoint joinPoint, Consumer<String> logCallback) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

        logCallback.accept(String.format(METHOD_RETURN_TYPE, methodSignature.getReturnType()));
        logCallback.accept(String.format(METHOD_NAME, methodSignature.getName()));

        Object[] args = joinPoint.getArgs();
        Arrays.stream(args).forEach(r -> logCallback.accept(String.format(METHOD_ARGUMENT_INFO_VALUE, r.getClass(), r)));

        Object returningValue = joinPoint.proceed();
        if (!methodSignature.getReturnType().equals(Void.TYPE)) {
            logCallback.accept(String.format(METHOD_RETURNING_VALUE_INFO, returningValue.getClass(), returningValue));
        } else {
            logCallback.accept(VOID_TYPE_METHOD);
        }
        return returningValue;
    }


    @Around(value = "@annotation(logError)")
    public void methodErrorAdvice(ProceedingJoinPoint joinPoint, LogError logError) throws Throwable {
        try {
            joinPoint.proceed();
        } catch (Throwable e) {
            log.error(logError.message());
            if (logError.swallowException() && e.getClass().isAssignableFrom(logError.exception()) && IOException.class.isAssignableFrom(logError.exception())) {
                log.error(String.format(EXCEPTION_WAS_CAUGHT, e.getClass().getName()));
            } else {
                log.error(String.format(EXCEPTION_WAS_THROWN, e.getClass().getName()));
                throw e;
            }

        }
    }
}
