package com.example.starter.config;

import com.example.starter.aop.LoggingMethodAspect;
import org.springframework.context.annotation.*;

@Configuration
public class LoggingAspectAutoConfiguration {

    @Bean
    public LoggingMethodAspect loggingMethodAspect(){
        return new LoggingMethodAspect();
    }
}
