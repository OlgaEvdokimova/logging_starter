package constant;

public class TestConstants {

    public static final String MESSAGE_FILE_NOT_FOUND_EXCEPTION = "java.io.FileNotFoundException";
    public static final String MESSAGE_ARITHMETIC_EXCEPTION = "java.lang.ArithmeticException";
    public static final String MESSAGE_CAUGHT = "To be caught";
    public static final String MESSAGE_THROWN = "To be thrown";
}
