package com.example.starter.test.integration;

import com.example.starter.annotation.LogError;
import com.example.starter.aop.LoggingMethodAspect;
import com.example.starter.config.LoggingAspectAutoConfiguration;
import com.example.starter.test.Config;
import com.example.starter.test.MyTest;
import com.google.common.collect.ImmutableList;
import constant.TestConstants;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import uk.org.lidalia.slf4jtest.LoggingEvent;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.starter.utils.TemplateUtils.*;
import static constant.TestConstants.MESSAGE_CAUGHT;
import static constant.TestConstants.MESSAGE_THROWN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {LoggingAspectAutoConfiguration.class, Config.class})
public class TestAspectLogging {

    @Autowired
    private MyTest myTest;

    TestLogger logger = TestLoggerFactory.getTestLogger(LoggingMethodAspect.class);

    @Test
    public void shouldCheckLoggingInMethodTest_WithAnnotation_LogInfo() throws NoSuchMethodException {
        Method test = MyTest.class.getMethod("test", String.class);

        Parameter[] parameters = test.getParameters();
        List<Parameter> parameterList = Arrays.asList(parameters);

        String str = "123";
        String returnMethodValue = myTest.test(str);

        List<String> expectedMessages = List.of(
                METHOD_INFO_INFORMATION,
                String.format(METHOD_RETURN_TYPE, test.getReturnType()),
                String.format(METHOD_NAME, test.getName()),
                String.format(METHOD_ARGUMENT_INFO_VALUE, parameterList.get(0).getName().getClass(), str),
                String.format(METHOD_RETURNING_VALUE_INFO, returnMethodValue.getClass(), returnMethodValue)
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

    }


    @Test
    public void shouldCheckLoggingInMethodTestVoid_WithAnnotation_LogDebug() throws NoSuchMethodException {
        Method testVoid = MyTest.class.getMethod("testVoid", String.class, String.class);
        Parameter[] parameters = testVoid.getParameters();
        List<Parameter> parameterList = Arrays.asList(parameters);

        String str1 = "123";
        String str2 = "124";

        myTest.testVoid(str1, str2);

        List<String> expectedMessages = List.of(
                METHOD_DEBUG_INFORMATION,
                String.format(METHOD_RETURN_TYPE, testVoid.getReturnType()),
                String.format(METHOD_NAME, testVoid.getName()),
                String.format(METHOD_ARGUMENT_INFO_VALUE, parameterList.get(0).getName().getClass(), str1),
                String.format(METHOD_ARGUMENT_INFO_VALUE, parameterList.get(1).getName().getClass(), str2),
                VOID_TYPE_METHOD
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

    }

    @Test
    public void shouldThrowArithmeticException() throws NoSuchMethodException {
        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, TestConstants.MESSAGE_ARITHMETIC_EXCEPTION)
        );

        assertThrows(ArithmeticException.class, () -> myTest.testException());

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);
    }

    @Test
    public void shouldCaughtFileNotFoundException_WithArguments_SwallowTrue_And_ExceptionFileNotFound() throws NoSuchMethodException, IOException {
        List<String> expectedMessages = List.of(
                MESSAGE_CAUGHT,
                String.format(EXCEPTION_WAS_CAUGHT, TestConstants.MESSAGE_FILE_NOT_FOUND_EXCEPTION)
        );

        myTest.testFNFExceptionCaught();

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);
    }

    @Test
    public void shouldThrowFileNotFoundException_WithArguments_SwallowFalse_And_ExceptionFileNotFound()  {
        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, TestConstants.MESSAGE_FILE_NOT_FOUND_EXCEPTION)
        );
        assertThrows(FileNotFoundException.class, () -> myTest.testFNFExceptionThrown1());

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);
    }

    @Test
    public void shouldThrowFileNotFound_WithArguments_SwallowTrue_And_ExceptionError()   {

        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, TestConstants.MESSAGE_FILE_NOT_FOUND_EXCEPTION)
                );

        assertThrows(FileNotFoundException.class, () -> myTest.testFNFExceptionThrown2());

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);
    }


    @AfterEach
    public void clearLoggers() {
        TestLoggerFactory.clear();
    }
}
