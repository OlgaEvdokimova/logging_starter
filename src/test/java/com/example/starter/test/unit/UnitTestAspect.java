package com.example.starter.test.unit;

import com.example.starter.annotation.LogError;
import com.example.starter.aop.LoggingMethodAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.stereotype.Component;
import uk.org.lidalia.slf4jtest.LoggingEvent;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.starter.utils.TemplateUtils.*;
import static constant.TestConstants.MESSAGE_CAUGHT;
import static constant.TestConstants.MESSAGE_THROWN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

@Component
@ExtendWith(MockitoExtension.class)
public class UnitTestAspect {

    @Mock
    private ProceedingJoinPoint joinPoint;
    @Mock
    private MethodSignature methodSignature;

    @InjectMocks
    private LoggingMethodAspect loggingMethodAspect;

    TestLogger logger = TestLoggerFactory.getTestLogger(LoggingMethodAspect.class);

    @Test
    void shouldCheckMethodInfoAdvice_With_ReturnValue() throws Throwable {
        Object returnValue = new Object();
        Object[] objects = {"123"};
        String name = "name";

        given(joinPoint.getSignature()).willReturn(methodSignature);
        given(joinPoint.getArgs()).willReturn(objects);
        given(joinPoint.proceed()).willReturn(returnValue);

        given(methodSignature.getReturnType()).willReturn(Class.class);
        given(methodSignature.getName()).willReturn(name);

        List<String> expectedMessages = List.of(
                METHOD_INFO_INFORMATION,
                String.format(METHOD_RETURN_TYPE, Class.class),
                String.format(METHOD_NAME, name),
                String.format(METHOD_ARGUMENT_INFO_VALUE, objects[0].getClass(), objects[0]),
                String.format(METHOD_RETURNING_VALUE_INFO, returnValue.getClass(), returnValue)
        );
        assertEquals(returnValue, loggingMethodAspect.methodInfoAdvice(joinPoint));

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(times(1)).getSignature();
        then(joinPoint).should(times(1)).getArgs();
        then(joinPoint).should(times(1)).proceed();
        then(joinPoint).shouldHaveNoMoreInteractions();

        then(methodSignature).should(times(2)).getReturnType();
        then(methodSignature).should(times(1)).getName();
        then(methodSignature).shouldHaveNoMoreInteractions();
    }

    @Test
    void shouldCheckMethodInfoAdvice_With_ReturnVoidType() throws Throwable {
        Object[] objects = {"123"};
        String name = "name";

        given(joinPoint.getSignature()).willReturn(methodSignature);
        given(joinPoint.getArgs()).willReturn(objects);

        given(methodSignature.getReturnType()).willReturn(Void.TYPE);
        given(methodSignature.getName()).willReturn(name);

        List<String> expectedMessages = List.of(
                METHOD_INFO_INFORMATION,
                String.format(METHOD_RETURN_TYPE, Void.TYPE),
                String.format(METHOD_NAME, name),
                String.format(METHOD_ARGUMENT_INFO_VALUE, objects[0].getClass(), objects[0]),
                VOID_TYPE_METHOD
        );
        loggingMethodAspect.methodInfoAdvice(joinPoint);

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(times(1)).getSignature();
        then(joinPoint).should(times(1)).getArgs();
        then(joinPoint).should(times(1)).proceed();
        then(joinPoint).shouldHaveNoMoreInteractions();

        then(methodSignature).should(times(2)).getReturnType();
        then(methodSignature).should(times(1)).getName();
        then(methodSignature).shouldHaveNoMoreInteractions();
    }


    @Test
    void shouldCheckMethodDebugAdvice_With_ReturnVoidType() throws Throwable {

        Object[] objects = {"123"};
        String name = "name";

        given(joinPoint.getSignature()).willReturn(methodSignature);
        given(joinPoint.getArgs()).willReturn(objects);

        given(methodSignature.getReturnType()).willReturn(Void.TYPE);
        given(methodSignature.getName()).willReturn(name);

        List<String> expectedMessages = List.of(
                METHOD_DEBUG_INFORMATION,
                String.format(METHOD_RETURN_TYPE, Void.TYPE),
                String.format(METHOD_NAME, name),
                String.format(METHOD_ARGUMENT_INFO_VALUE, objects[0].getClass(), objects[0]),
                VOID_TYPE_METHOD
        );
        loggingMethodAspect.methodDebugAdvice(joinPoint);

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(times(1)).getSignature();
        then(joinPoint).should(times(1)).getArgs();
        then(joinPoint).should(times(1)).proceed();
        then(joinPoint).shouldHaveNoMoreInteractions();

        then(methodSignature).should(times(2)).getReturnType();
        then(methodSignature).should(times(1)).getName();
        then(methodSignature).shouldHaveNoMoreInteractions();
    }

    @Test
    void shouldCheckMethodDebugAdvice_With_ReturnValue() throws Throwable {
        Object returnValue = new Object();
        Object[] objects = {"123"};
        String name = "name";

        given(joinPoint.getSignature()).willReturn(methodSignature);
        given(joinPoint.getArgs()).willReturn(objects);
        given(joinPoint.proceed()).willReturn(returnValue);

        given(methodSignature.getReturnType()).willReturn(Class.class);
        given(methodSignature.getName()).willReturn(name);

        List<String> expectedMessages = List.of(
                METHOD_DEBUG_INFORMATION,
                String.format(METHOD_RETURN_TYPE, Class.class),
                String.format(METHOD_NAME, name),
                String.format(METHOD_ARGUMENT_INFO_VALUE, objects[0].getClass(), objects[0]),
                String.format(METHOD_RETURNING_VALUE_INFO, returnValue.getClass(), returnValue)
        );
        assertEquals(returnValue, loggingMethodAspect.methodDebugAdvice(joinPoint));

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(times(1)).getSignature();
        then(joinPoint).should(times(1)).getArgs();
        then(joinPoint).should(times(1)).proceed();
        then(joinPoint).shouldHaveNoMoreInteractions();

        then(methodSignature).should(times(2)).getReturnType();
        then(methodSignature).should(times(1)).getName();
        then(methodSignature).shouldHaveNoMoreInteractions();
    }


    @Test
    void should_Throw_FileNotFoundException_Check_MethodErrorAdvice() throws Throwable {

        Method testMethod = UnitTestAspect.class.getMethod("methodThrowFileNotFoundException");
        LogError logError = testMethod.getAnnotation(LogError.class);

        given(joinPoint.proceed()).willThrow(FileNotFoundException.class);

        FileNotFoundException fileNotFoundException = assertThrows(FileNotFoundException.class, () -> loggingMethodAspect.methodErrorAdvice(joinPoint, logError));

        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, fileNotFoundException.getClass().getName())
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(only()).proceed();
        then(methodSignature).shouldHaveNoInteractions();
    }

    @Test
    void should_Catch_FileNotFoundException_Check_MethodErrorAdvice() throws Throwable {

        Method testMethod = UnitTestAspect.class.getMethod("methodCaughtFileNotFoundException");
        LogError logError = testMethod.getAnnotation(LogError.class);

        given(joinPoint.proceed()).willThrow(FileNotFoundException.class);

       loggingMethodAspect.methodErrorAdvice(joinPoint, logError);

        List<String> expectedMessages = List.of(
                MESSAGE_CAUGHT,
                String.format(EXCEPTION_WAS_CAUGHT, FileNotFoundException.class.getName())
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(only()).proceed();
        then(methodSignature).shouldHaveNoInteractions();
    }

    @Test
    void should_Throw_ArithmeticException_Check_MethodErrorAdvice() throws Throwable {

        Method testMethod = UnitTestAspect.class.getMethod("methodThrowArithmeticExceptionException");
        LogError logError = testMethod.getAnnotation(LogError.class);

        given(joinPoint.proceed()).willThrow(ArithmeticException.class);

        ArithmeticException arithmeticException = assertThrows(ArithmeticException.class, () -> loggingMethodAspect.methodErrorAdvice(joinPoint, logError));

        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, arithmeticException.getClass().getName())
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(only()).proceed();
        then(methodSignature).shouldHaveNoInteractions();
    }

    @Test
    void should_Throw_ErrorException_Check_MethodErrorAdvice() throws Throwable {

        Method testMethod = UnitTestAspect.class.getMethod("methodThrowErrorException");
        LogError logError = testMethod.getAnnotation(LogError.class);

        given(joinPoint.proceed()).willThrow(Error.class);

        Error error = assertThrows(Error.class, () -> loggingMethodAspect.methodErrorAdvice(joinPoint, logError));

        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, error.getClass().getName())
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(only()).proceed();
        then(methodSignature).shouldHaveNoInteractions();
    }

    @Test
    void should_Throw_ErrorException_2_Check_MethodErrorAdvice() throws Throwable {

        Method testMethod = UnitTestAspect.class.getMethod("methodThrowErrorException2");
        LogError logError = testMethod.getAnnotation(LogError.class);

        given(joinPoint.proceed()).willThrow(Error.class);

        Error error = assertThrows(Error.class, () -> loggingMethodAspect.methodErrorAdvice(joinPoint, logError));

        List<String> expectedMessages = List.of(
                MESSAGE_THROWN,
                String.format(EXCEPTION_WAS_THROWN, error.getClass().getName())
        );

        List<String> actualMessages = logger.getLoggingEvents().asList()
                .stream()
                .map(LoggingEvent::getMessage)
                .collect(Collectors.toList());

        assertEquals(expectedMessages, actualMessages);

        then(joinPoint).should(only()).proceed();
        then(methodSignature).shouldHaveNoInteractions();
    }

    @LogError(message = MESSAGE_CAUGHT, exception = FileNotFoundException.class, swallowException = true)
    public void methodCaughtFileNotFoundException() throws FileNotFoundException {
        throw new FileNotFoundException();
    }
    @LogError(message = MESSAGE_THROWN, exception = FileNotFoundException.class)
    public void methodThrowFileNotFoundException() throws FileNotFoundException {
        throw new FileNotFoundException();
    }
    @LogError(message = MESSAGE_THROWN, exception = ArithmeticException.class)
    public void methodThrowArithmeticExceptionException() {
        throw new ArithmeticException();
    }
    @LogError(message = MESSAGE_THROWN, exception = Error.class, swallowException = true)
    public void methodThrowErrorException() {
        throw new Error();
    }

    @LogError(message = MESSAGE_THROWN, exception = FileNotFoundException.class, swallowException = true)
    public void methodThrowErrorException2(){
        throw new Error();
    }

    @AfterEach
    public void clearLoggers() {
        TestLoggerFactory.clear();
    }
}
