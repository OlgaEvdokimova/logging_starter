package com.example.starter.test;

import com.example.starter.annotation.LogDebug;
import com.example.starter.annotation.LogError;
import com.example.starter.annotation.LogInfo;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class MyTest {

    @LogInfo
    public String test(String str) {
        return str;
    }

    @LogDebug
    public void testVoid(String str1, String str2) {
        System.out.println(str1 + str2);
    }

    @LogError(message = "To be thrown")
    public void testException() {
        throw new ArithmeticException();
    }

    @LogError(exception = FileNotFoundException.class, swallowException = true, message = "To be caught")
    public void testFNFExceptionCaught() throws FileNotFoundException {
       throw new FileNotFoundException();
    }

    @LogError(exception = FileNotFoundException.class, message = "To be thrown")
    public void testFNFExceptionThrown1() throws FileNotFoundException {
        throw new FileNotFoundException();
    }

    @LogError(exception = Error.class, swallowException = true, message = "To be thrown")
    public void testFNFExceptionThrown2() throws FileNotFoundException {
        throw new FileNotFoundException();
    }
}
